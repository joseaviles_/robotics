package robotics;

import java.util.ArrayList;
import javax.swing.JLabel;

public class chronometer_thread extends Thread
{
    public JLabel chronometer;
    public JLabel alarm;
    public ArrayList<String> pieces;
    
    //chronometer
    public JLabel getChronometer()
    {
        return chronometer;
    }
    
    public void setChronometer(JLabel chronometer) 
    {
        this.chronometer = chronometer;
    }
    
    //alarm
    public JLabel getAlarm()
    {
        return alarm;
    }
    
    public void setAlarm(JLabel alarm) 
    {
        this.alarm = alarm;
    }
    
    //pieces
    public ArrayList getPieces()
    {
        return pieces;
    }
    
    public void setPieces(ArrayList pieces)
    {
        this.pieces = pieces;
    }
}
