package robotics;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class chronometer_controller extends chronometer_thread
{
    public chronometer_controller(JLabel chronometer, JLabel alarm, ArrayList<String> pieces)
    {
        setChronometer(chronometer);
        setAlarm(alarm); 
        setPieces(pieces);
    }
    
    @Override
    public void run()
    {
        int count=0;
        int finish=getPieces().size();
        int seconds=0;
        
        //move the robot
        while(count<finish)
        {
            if(getPieces().get(count)=="red"){
                seconds+=4;
            }else if(getPieces().get(count)=="black"){
                seconds+=5;
            }else if(getPieces().get(count)=="aluminum"){
                seconds+=5;
            }
            count++;
        }
        
        count=0;
        seconds=seconds-1;
        
        ImageIcon image = new ImageIcon(getClass().getResource("/img/alarm-on.gif"));
        getAlarm().setIcon(image);
        
        while(count<seconds)
        {
            try
            {
                Thread.sleep(1000);
                count++;
                getChronometer().setText(String.valueOf(count)+" s");
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
        ImageIcon newImage = new ImageIcon(getClass().getResource("/img/alarm-off.png"));
        getAlarm().setIcon(newImage);
    }
}
