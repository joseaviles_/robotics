package robotics;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class robot_controller extends robot_thread 
{
    public robot_controller(JLabel robot, JLabel piece, JLabel extraPiece, JLabel countRed, JLabel countBlack, JLabel countAluminum, JLabel finalMsg, int xRobot, int yRobot, int xPiece, int yPiece, int xExtraPiece, int yExtraPiece, ArrayList<String> pieces)
    {
        setRobot(robot);
        setPiece(piece);
        setExtraPiece(extraPiece);
        setCountRed(countRed);
        setCountBlack(countBlack);
        setCountAluminum(countAluminum);
        setFinalMsg(finalMsg);
        setXRobot(xRobot);
        setYRobot(yRobot);
        setXPiece(xPiece);
        setYPiece(yPiece);
        setXExtraPiece(xExtraPiece);
        setYExtraPiece(yExtraPiece);
        setPieces(pieces);
    }
    
    @Override
    public void run()
    {
        int finish=getPieces().size();
        int count=0;
        int count_red=0;
        int count_black=0;
        int count_aluminum=0;
        
        getExtraPiece().setVisible(true);
        
        //move the robot
        while(count<finish)
        {
            
            if(getPieces().get(count)=="red")
            {
                ImageIcon image = new ImageIcon(getClass().getResource("/img/red_piece.png"));
                getPiece().setIcon(image);
                getExtraPiece().setIcon(image);
            }
            else if(getPieces().get(count)=="black")
            {
                ImageIcon image = new ImageIcon(getClass().getResource("/img/black_piece.png"));
                getPiece().setIcon(image);
                getExtraPiece().setIcon(image);
            }
            else if(getPieces().get(count)=="aluminum")
            {
                ImageIcon image = new ImageIcon(getClass().getResource("/img/aluminum_piece.png"));
                getPiece().setIcon(image);
                getExtraPiece().setIcon(image);
            }
            
            getPiece().setVisible(false);
            
            //left ribbon
            for(int i=0; i<100; i++)
            {
                try
                {
                    setXExtraPiece(getXExtraPiece()-i);
                    getExtraPiece().setLocation(getXExtraPiece(),getYExtraPiece());
                    Thread.sleep(50);
                    
                    if(getXExtraPiece()<782)
                    {
                        getPiece().setVisible(true);
                        break;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            
            //hide extra piece
            getExtraPiece().setVisible(false);
            
            //right ribbon
            for(int i=0; i<100; i++)
            {
                try
                {
                    setXExtraPiece(getXExtraPiece()+i);
                    getExtraPiece().setLocation(getXExtraPiece(),getYExtraPiece());
                    
                    if(getXExtraPiece()>940)
                    {
                        break;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            
            //left robot
            for(int i=0; i<100; i++)
            {
                try
                {
                    setXRobot(getXRobot()-i);
                    setXPiece(getXPiece()-i);
                    getRobot().setLocation(getXRobot(),getYRobot());
                    getPiece().setLocation(getXPiece(),getYPiece());
                    Thread.sleep(50);
                    
                    //repository place
                    //535:(1)red - 375:(2)black - 215:(3)aluminun
                    
                    int limit=0;

                    if(getPieces().get(count)=="red")
                        limit=535;
                    else if(getPieces().get(count)=="black")
                        limit=375;
                    else if(getPieces().get(count)=="aluminum")
                        limit=215;
                    
                    if(getXRobot()<limit)
                    {
                        break;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            
            //save piece
            for(int i=0; i<100; i++)
            {
                try
                {
                    setYPiece(getYPiece()+i);
                    getPiece().setLocation(getXPiece(),getYPiece());
                    Thread.sleep(50);
                    
                    if(getYPiece()>350)
                    {
                        if(getPieces().get(count)=="red")
                        {
                            count_red++;
                            getCountRed().setText(String.valueOf(count_red));
                        }
                        else if(getPieces().get(count)=="black")
                        {
                            count_black++;
                            getCountBlack().setText(String.valueOf(count_black));
                        }
                        else if(getPieces().get(count)=="aluminum")
                        {
                            count_aluminum++;
                            getCountAluminum().setText(String.valueOf(count_aluminum));
                        }
                        
                        break;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            
            //hide piece
            getPiece().setVisible(false);
            
            //right robot
            for(int i=0; i<100; i++)
            {
                try
                {
                    setXRobot(getXRobot()+i);
                    setXPiece(getXPiece()+i);
                    getRobot().setLocation(getXRobot(),getYRobot());
                    getPiece().setLocation(getXPiece(),getYPiece());
                    Thread.sleep(50);
                    
                    if(getXRobot()>750)
                    {
                        //restart position of piece
                        setYPiece(240);
                        getPiece().setLocation(getXPiece(),getYPiece());
                        getExtraPiece().setVisible(true);
                        break;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            count++;
        }
        getExtraPiece().setVisible(false);
        getFinalMsg().setVisible(true);
    }
}
