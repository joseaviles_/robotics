package robotics;

import java.awt.Image;
import java.awt.Toolkit;
import static java.lang.System.exit;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class start extends javax.swing.JFrame {
    ArrayList<String> pieces = new ArrayList<String>();
    ArrayList<String> extra_pieces = new ArrayList<String>();

    public start() {
        initComponents();
        this.setLocationRelativeTo(null);
        piece.setVisible(false);
        extra_piece.setVisible(false);
        final_msg.setVisible(false);
    }
    
    //ICONO DE LA VENTANA
    @Override
    public Image getIconImage() 
    {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("img/icono.png"));
        return retValue;
    }
    
    public boolean isNumeric(String string){
    	try 
        {
            Integer.parseInt(string);
            return true;
    	} 
        catch(NumberFormatException nfe)
        {
            return false;
    	}
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        count_red = new javax.swing.JLabel();
        red = new javax.swing.JLabel();
        count_aluminum = new javax.swing.JLabel();
        aluminum = new javax.swing.JLabel();
        count_black = new javax.swing.JLabel();
        black = new javax.swing.JLabel();
        extra_repository = new javax.swing.JLabel();
        piece = new javax.swing.JLabel();
        extra_piece = new javax.swing.JLabel();
        pieces_total = new javax.swing.JLabel();
        conveyer_belt = new javax.swing.JLabel();
        robot = new javax.swing.JLabel();
        start = new javax.swing.JButton();
        restart = new javax.swing.JButton();
        quit = new javax.swing.JButton();
        rail = new javax.swing.JLabel();
        timer = new javax.swing.JLabel();
        chron = new javax.swing.JLabel();
        floor = new javax.swing.JLabel();
        final_msg = new javax.swing.JLabel();
        alarm = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Robótica");
        setIconImage(getIconImage());
        setMinimumSize(new java.awt.Dimension(1000, 600));
        setResizable(false);
        setSize(new java.awt.Dimension(1000, 600));
        getContentPane().setLayout(null);

        count_red.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        count_red.setForeground(new java.awt.Color(255, 255, 255));
        count_red.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        count_red.setText("0");
        count_red.setToolTipText("");
        getContentPane().add(count_red);
        count_red.setBounds(490, 370, 130, 14);

        red.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        red.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/red.png"))); // NOI18N
        getContentPane().add(red);
        red.setBounds(490, 354, 131, 87);

        count_aluminum.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        count_aluminum.setForeground(new java.awt.Color(255, 255, 255));
        count_aluminum.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        count_aluminum.setText("0");
        count_aluminum.setToolTipText("");
        getContentPane().add(count_aluminum);
        count_aluminum.setBounds(170, 370, 130, 14);

        aluminum.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        aluminum.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/aluminum.png"))); // NOI18N
        getContentPane().add(aluminum);
        aluminum.setBounds(170, 354, 131, 87);

        count_black.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        count_black.setForeground(new java.awt.Color(255, 255, 255));
        count_black.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        count_black.setText("0");
        count_black.setToolTipText("");
        getContentPane().add(count_black);
        count_black.setBounds(330, 370, 130, 14);

        black.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        black.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/black.png"))); // NOI18N
        getContentPane().add(black);
        black.setBounds(330, 354, 131, 87);

        extra_repository.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        extra_repository.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/extra.png"))); // NOI18N
        getContentPane().add(extra_repository);
        extra_repository.setBounds(10, 354, 131, 87);

        piece.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        piece.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/red_piece.png"))); // NOI18N
        getContentPane().add(piece);
        piece.setBounds(782, 240, 40, 72);

        extra_piece.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        extra_piece.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/red_piece.png"))); // NOI18N
        getContentPane().add(extra_piece);
        extra_piece.setBounds(950, 240, 40, 72);

        pieces_total.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        pieces_total.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pieces_total.setText("0 piezas");
        pieces_total.setToolTipText("");
        getContentPane().add(pieces_total);
        pieces_total.setBounds(660, 350, 340, 30);

        conveyer_belt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        conveyer_belt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/conveyer_belt.png"))); // NOI18N
        getContentPane().add(conveyer_belt);
        conveyer_belt.setBounds(640, 306, 596, 135);

        robot.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/robot.png"))); // NOI18N
        getContentPane().add(robot);
        robot.setBounds(770, 80, 231, 361);

        start.setText("Iniciar");
        start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startActionPerformed(evt);
            }
        });
        getContentPane().add(start);
        start.setBounds(10, 10, 61, 23);

        restart.setText("Reiniciar");
        restart.setEnabled(false);
        restart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restartActionPerformed(evt);
            }
        });
        getContentPane().add(restart);
        restart.setBounds(80, 10, 73, 23);

        quit.setText("Salir");
        quit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitActionPerformed(evt);
            }
        });
        getContentPane().add(quit);
        quit.setBounds(910, 10, 73, 23);

        rail.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/rail.png"))); // NOI18N
        getContentPane().add(rail);
        rail.setBounds(0, 426, 1000, 15);

        timer.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        timer.setForeground(new java.awt.Color(255, 0, 0));
        timer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        timer.setText("0 s");
        getContentPane().add(timer);
        timer.setBounds(420, 490, 150, 50);

        chron.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chron.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/timer.png"))); // NOI18N
        getContentPane().add(chron);
        chron.setBounds(420, 490, 150, 50);

        floor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        floor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/floor.png"))); // NOI18N
        getContentPane().add(floor);
        floor.setBounds(0, 440, 1000, 160);

        final_msg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        final_msg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/final_msg.png"))); // NOI18N
        getContentPane().add(final_msg);
        final_msg.setBounds(230, 60, 536, 60);

        alarm.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        alarm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/alarm-off.png"))); // NOI18N
        getContentPane().add(alarm);
        alarm.setBounds(45, 50, 60, 60);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startActionPerformed
        String resp = JOptionPane.showInputDialog("Ingresa la cantidad de piezas para la simulación");
        
        if(isNumeric(resp))
        {
            int result = Integer.parseInt(resp);
            if(result<=0)
            {
                JOptionPane.showMessageDialog(null, "La cantidad de piezas debe ser un número entero mayor a cero (0).");
            }
            else
            {
                for(int i=0; i<result; i++)
                {
                    int number = (int) (Math.random() * 3) + 1;
                   
                    if(number==1){
                        pieces.add("red");
                        extra_pieces.add("red");
                    }else if(number==2){
                        pieces.add("black");
                        extra_pieces.add("black");
                    }else if(number==3){
                        pieces.add("aluminum");
                        extra_pieces.add("aluminum");
                    }
                }
                
                robot_controller new_robot = new robot_controller(robot,piece,extra_piece,count_red,count_black,count_aluminum,final_msg,robot.getX(),robot.getY(),piece.getX(),piece.getY(),extra_piece.getX(),extra_piece.getY(),pieces);
                new_robot.start();
                
                chronometer_controller new_chronometer = new chronometer_controller(timer,alarm,extra_pieces);
                new_chronometer.start();
                
                start.setEnabled(false);
                restart.setEnabled(true);
                pieces_total.setText(resp+" piezas");
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "La cantidad de piezas debe ser un número entero mayor a cero (0).");
        }
    }//GEN-LAST:event_startActionPerformed

    private void restartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restartActionPerformed
        this.setVisible(false);
        start new_simulation = new start();
        new_simulation.setVisible(true);
    }//GEN-LAST:event_restartActionPerformed

    private void quitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitActionPerformed
        exit(0);
    }//GEN-LAST:event_quitActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(start.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(start.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(start.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(start.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new start().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel alarm;
    private javax.swing.JLabel aluminum;
    private javax.swing.JLabel black;
    private javax.swing.JLabel chron;
    public javax.swing.JLabel conveyer_belt;
    public javax.swing.JLabel count_aluminum;
    public javax.swing.JLabel count_black;
    public javax.swing.JLabel count_red;
    public javax.swing.JLabel extra_piece;
    private javax.swing.JLabel extra_repository;
    public javax.swing.JLabel final_msg;
    private javax.swing.JLabel floor;
    public javax.swing.JLabel piece;
    public javax.swing.JLabel pieces_total;
    private javax.swing.JButton quit;
    private javax.swing.JLabel rail;
    private javax.swing.JLabel red;
    private javax.swing.JButton restart;
    public javax.swing.JLabel robot;
    private javax.swing.JButton start;
    public javax.swing.JLabel timer;
    // End of variables declaration//GEN-END:variables
}
