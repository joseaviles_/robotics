package robotics;

import java.util.ArrayList;
import javax.swing.JLabel;

public class robot_thread extends Thread 
{
    public JLabel robot;
    public JLabel piece;
    public JLabel countRed;
    public JLabel countBlack;
    public JLabel countAluminum;
    public JLabel finalMsg;
    public int xRobot;
    public int yRobot;
    public int xPiece;
    public int yPiece;
    public JLabel extraPiece;
    public int xExtraPiece;
    public int yExtraPiece;
    public ArrayList<String> pieces;
    
    //robot
    public JLabel getRobot()
    {
        return robot;
    }
    
    public void setRobot(JLabel robot) 
    {
        this.robot = robot;
    }
    
    //piece
    public JLabel getPiece()
    {
        return piece;
    }
    
    public void setPiece(JLabel piece) 
    {
        this.piece = piece;
    }
    
    //extra piece
    public JLabel getExtraPiece()
    {
        return extraPiece;
    }
    
    public void setExtraPiece(JLabel extraPiece) 
    {
        this.extraPiece = extraPiece;
    }
    
    //countRed
    public JLabel getCountRed()
    {
        return countRed;
    }
    
    public void setCountRed(JLabel countRed) 
    {
        this.countRed = countRed;
    }
    
    //countBlack
    public JLabel getCountBlack()
    {
        return countBlack;
    }
    
    public void setCountBlack(JLabel countBlack) 
    {
        this.countBlack = countBlack;
    }
    
    //countAluminum
    public JLabel getCountAluminum()
    {
        return countAluminum;
    }
    
    public void setCountAluminum(JLabel countAluminum) 
    {
        this.countAluminum = countAluminum;
    }
    
    //finalMsg
    public JLabel getFinalMsg()
    {
        return finalMsg;
    }
    
    public void setFinalMsg(JLabel finalMsg) 
    {
        this.finalMsg = finalMsg;
    }
    
    //xRobot
    public int getXRobot()
    {
        return xRobot;
    }
    
    public void setXRobot(int xRobot) 
    {
        this.xRobot = xRobot;
    }
    
    //yRobot
    public int getYRobot()
    {
        return yRobot;
    }
    
    public void setYRobot(int yRobot) 
    {
        this.yRobot = yRobot;
    }
    
    //xPiece
    public int getXPiece()
    {
        return xPiece;
    }
    
    public void setXPiece(int xPiece) 
    {
        this.xPiece = xPiece;
    }
    
    //yPiece
    public int getYPiece()
    {
        return yPiece;
    }
    
    public void setYPiece(int yPiece) 
    {
        this.yPiece = yPiece;
    }
    
    //xExtraPiece
    public int getXExtraPiece()
    {
        return xExtraPiece;
    }
    
    public void setXExtraPiece(int xExtraPiece) 
    {
        this.xExtraPiece = xExtraPiece;
    }
    
    //yExtraPiece
    public int getYExtraPiece()
    {
        return yExtraPiece;
    }
    
    public void setYExtraPiece(int yExtraPiece) 
    {
        this.yExtraPiece = yExtraPiece;
    }
    
    //pieces
    public ArrayList getPieces()
    {
        return pieces;
    }
    
    public void setPieces(ArrayList pieces)
    {
        this.pieces = pieces;
    }
}
